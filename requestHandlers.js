var exec = require('child_process').exec;
var fs = require('fs');
var url = require('url');
// test for git

function start(request, response){
    console.log("<Router> Request handler was called by 'start'");
    
    response.writeHead(200, {"Content-Type" : "text/html"});
    var index;
    index = fs.readFileSync('src/index.html');
    response.write(index.toString());    
    response.end();
    // exec("ls -lah", function(error, stdout, stderr){
    //     response.writeHead(200, {"Content-Type" : "text/plain"});
    //     if(error) {
    //         response.write("Command Fail");
    //         response.end();
    //     }
    //     else {
    //         response.write(stdout);
    //         response.end();
    //     }
    // });
}

function upload(request, response){
    console.log("<Router> Request handler was called by 'upload'");
    response.writeHead(200, {"Content-Type" : "text/html"});
    _upload = fs.readFileSync('src/upload.html');
    _upload = _upload.toString();
    _upload = _upload.replace("{{name}}", url.parse(request.url, true).query['name'])
    response.write(_upload);
    response.end();
}

function favicon(request, response) {
    console.log("<Router> Request handler was called by 'favicon'");
    response.writeHead(200, {"content-Type" : "image/ico"});
    favicon_img = fs.readFileSync('src/favicon.ico');
    response.write(favicon_img);
    response.end();
}

function Not_Found(request, response) {
    console.log('<Router> No request handler found for' + request.url);
    response.writeHead(404, {"Content-Type" : "text/plain"});
    response.write("404 Not Found");
    response.end();
}

exports.favicon = favicon;
exports.start = start;
exports.upload = upload;
exports.Not_Found = Not_Found;