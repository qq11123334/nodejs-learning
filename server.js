var http = require('http');
var url = require('url');
var fs = require('fs');

var port = 8000;
function start(route, handle){
    console.log('<Server> Run at Port 8000')
    function make_request(request, response){

        var pathname = url.parse(request.url, true).pathname;
        console.log('<Server> Receive request for' + pathname + ' by ' + request.method);
        route(pathname, handle, request, response);
    }
    http.createServer(make_request).listen(port);
}
exports._start = start;