function route(pathname, handle, request, response){
    console.log('<Router> Path' + pathname);

    if(typeof handle[pathname] === 'function'){
        handle[pathname](request, response);
    } else {
        handle['Not_Found'](request, response);
    }
}

exports.route = route