var server = require('./server');
var router = require('./route');
var requestHandlers = require('./requestHandlers');

var handle = {};

handle['/'] = requestHandlers.start;
handle['/start'] = requestHandlers.start;
handle['/upload'] = requestHandlers.upload;
handle['/favicon.ico'] = requestHandlers.favicon;
handle['Not_Found'] = requestHandlers.Not_Found;

server._start(router.route, handle);
console.log('<Server> Start');
